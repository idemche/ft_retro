#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "GameObject.hpp"

class Player : public GameObject {
    
    public:

        Player(Player::GameObjectParameters &parameters,
        Player::GameObjectWindowParameters &windowParameters,
        int &lives);
        virtual ~Player();
        void    refresh(void);
        void    moveWithPosition(int dX, int dY);
        const unsigned &livesLeft(void) const;

        void        Player::lostLife(void);
    
    private:

        Player(Player const &copy);
        Player &operator=(Player const &rhs);
        
        unsigned        _lives;
        virtual void    _attack(void);
};

#endif
