#ifndef ENEMY_HPP
# define ENEMY_HPP

#include "GameObject.hpp"

class Enemy : public GameObject {

    public:
        typedef struct EnemyParameters {
            unsigned damage;
            unsigned score;
            uintmax_t lastShooted;
        }               EnemyParameters;
    
    public:
        Enemy();
        Enemy(EnemyParameters &parameters, int x, int y, int maxHP);
        Enemy(Enemy const &copy);
        virtual ~Enemy();
        Enemy &operator=(Enemy const &rhs);

        EnemyParameters &getEnemyParameters(void) const;

        virtual void attack(void);
        virtual void refresh(void);
    
    private:
        EnemyParameters _enemyParameters;
};


#endif
