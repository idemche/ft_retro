#ifndef ASTEROID_HPP
# define ASTEROID_HPP
# include "GameObject.hpp"

class Asteroid : public GameObject {
    public:
        Asteroid();
        Asteroid(int x, int y, int dX, int dY, int HP);
        virtual ~Asteroid();
        
        void refresh();
    
    private:
        virtual void attack(void);
        Asteroid(Asteroid const &copy);
        Asteroid &operator=(Asteroid const &rhs);
};

#endif