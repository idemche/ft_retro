#ifndef GAMEOBJECT_HPP
# define GAMEOBJECT_HPP
# include <iostream>
# include <cstdint>

class GameObject {

    protected:
        
        enum Colors {
            COLOR_BLACK,
            COLOR_RED,
            COLOR_GREEN,
            COLOR_YELLOW,
            COLOR_BLUE,
            COLOR_MAGENTA,
            COLOR_CYAN,
            COLOR_WHITE
        };

        typedef struct GameObjectParameters {
            std::string body;
            unsigned    damage;
            unsigned    maxHP;
            int         currentHP;
            Colors      color;
        }               GameObjectParameters;

        typedef struct GameObjectWindowParameters {
            int           x;
            int           y;
            int           dX;
            int           dY;
            unsigned      width;
            unsigned      height;
        }               GameObjectWindowParameters;

    public:
        GameObject(void);
        GameObject(GameObjectWindowParameters const &window,
        GameObjectParameters const &object);
        GameObject(GameObject const &copy);
        virtual ~GameObject();

        GameObject &operator=(GameObject const &rhs);
        GameObjectParameters const &getBody(void) const;
        GameObjectWindowParameters const &getWindowParameters() const;

        void    render(void);
        void    setPosition(int x, int y);
        void    moveToPosition(int dX, int dY);

        bool    hasHealth(void) const;
        void    restoreHealth(int newHealth);
        void    decreaseHealth(int damage);

     protected:
        
        GameObjectParameters        _objectParameters;
        GameObjectWindowParameters  _windowParameters;

        virtual void attack(void) = 0;
};

#endif
