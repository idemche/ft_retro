#include "ProgramNcurses.hpp"

ProgramNcurses::ProgramNcurses()
{
	this->_pState = MENU;
	startNcurses();
}

ProgramNcurses::ProgramNcurses(ProgramNcurses const &objToCopy)
{
	*this = objToCopy;
}

ProgramNcurses::~ProgramNcurses()
{
	endNcurses();
}

ProgramNcurses &ProgramNcurses::operator=(ProgramNcurses const &obj)
{
	if (this != &obj)
		;//	std::memcpy(this->_win, obj._win, sizeof(&obj._win));
	return (*this);
}


void ProgramNcurses::loopNcurses(void)
{
	int ch;

	while (this->_pState != EXIT)
	{
		if ((ch = getch()) == KEY_F(1))
			this->_pState = EXIT;
		wprintw(this->_menu->getWin(), "%c\n", ch);
	}
}

void ProgramNcurses::startGame()
{

}

void ProgramNcurses::startNcurses(void)
{
	this->_menu = new WindowNcurses(LINES - 1, COLS - 1, 0, 0, WINBUTTON);
	initscr();
	cbreak();
	keypad(stdscr, true);
	refresh();

}

void ProgramNcurses::endNcurses(void)
{
	delete this->_menu;
	endwin();
}
