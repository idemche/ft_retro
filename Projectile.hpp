#ifndef PROJECTILE_HPP
# define PROJECTILE_HPP

# include "GameObject.hpp"

class Projectile : public GameObject{
    public:
        Projectile();
        Projectile(GameObjectParameters &parameters,
        GameObjectWindowParameters &window);
        ~Projectile();
        Projectile(Projectile const &copy);
        Projectile &operator=(Projectile const &rhs);

        void refresh(void);
        void shift(void);

        private:
            virtual void attack(void);

};

#endif