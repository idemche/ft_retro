#include "Asteroid.hpp"

Asteroid::Asteroid(int x, int y, int dX, int dY, int HP) : GameObject(
(GameObjectWindowParameters){.x = x, .y = y, .dX = dX, .dY = dY},
(GameObjectParameters){"###\n###", .damage = 80, HP, HP, COLOR_MAGENTA}) {

}

Asteroid::Asteroid(Asteroid const &copy) {
    *this = copy;
}

Asteroid::~Asteroid() {}

Asteroid &Asteroid::operator=(Asteroid const &rhs) {
	static_cast<void>(rhs);
	return *this;
}

void Asteroid::refresh() {
	this->moveToPosition(
        this->_windowParameters.dX,
        this->_windowParameters.dY);
}

void Asteroid::attack() {}