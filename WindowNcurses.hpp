#ifndef WINDOWNCURSES_HPP
#define WINDOWNCURSES_HPP

#include <ncurses.h>
#include <iostream>

enum eWinStyles
{
	WINDEFAULT,
	WINBUTTON,
	WINGAME,
	WINSTATS
};

class WindowNcurses {
public:
	WindowNcurses();
	WindowNcurses(int height, int width);
	WindowNcurses(int height, int width, int startY, int startX, eWinStyles style);
	WindowNcurses(const WindowNcurses &objToCopy);
	~WindowNcurses();

	WindowNcurses &operator = (WindowNcurses const &obj);

	int getHeight() const;
	int getWidth() const;
	WINDOW *getWin() const;

	bool checkWindowDim(int x, int y);
	void redrawWindow(void);
	void sendMsg(const char *msg) const;

private:
	void windowCreate(void);
	void windowDelete(void);

	WINDOW *_win;
	int _height;
	int _width;
	int _startX;
	int _startY;
	eWinStyles _style;
	static const std::string _winBorders[4] ;
};

#endif
