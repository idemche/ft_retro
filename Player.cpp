#include "Player.hpp"

Player::Player(Player::GameObjectParameters &parameters,
Player::GameObjectWindowParameters &windowParameters, int &lives) {
    this->_windowParameters.x = windowParameters.x;
    this->_windowParameters.y = windowParameters.y;
    this->_objectParameters.maxHP = parameters.maxHP;
    this->_objectParameters.damage = parameters.damage;
    this->_lives = lives;
}

Player::~Player() {}

Player::Player(Player const &copy) {
    *this = copy;
}

Player &Player::operator=(Player const &rhs) {
    static_cast<void>(rhs);
    return *this;
}

const unsigned int   &Player::livesLeft(void) const {
    return this->_lives;
}

void        Player::moveWithPosition(int dX, int dY) {

    int x = dX + this->_windowParameters.x;
    int y = dY + this->_windowParameters.y;

    if (x > 0 && x > Level::mapWidth - this->_windowParameters.height - 1
        && y > 0 && y > Level::map - this->_windowParameters.width - 3) {
            this->_windowParameters.x = x;
            this->_windowParameters.y = y;
        }
}

void        Player::lostLife(void) {
    if (this->_lives > 0)
        this->_lives -= 1;
}

void        Player::refresh(void) {}