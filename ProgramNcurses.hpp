#ifndef PROGRAMNCURSES_HPP
#define PROGRAMNCURSES_HPP

#include <ncurses.h>
#include "WindowNcurses.hpp"

class ProgramNcurses
{
	enum eProgramState
	{
		MENU,
		GAME,
		EXIT
	};

public:
	ProgramNcurses();
	ProgramNcurses(ProgramNcurses const &objToCopy);
	~ProgramNcurses();

	ProgramNcurses &operator = (ProgramNcurses const &obj);

	void loopNcurses(void);
	void showMenu(void);
	void startGame();
	void exit();

private:
	void startNcurses(void);
	void endNcurses(void);

	WindowNcurses *_menu;
	WindowNcurses *_game;
	WindowNcurses *_stats;
	eProgramState _pState;
};

#endif
