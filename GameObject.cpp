#include "GameObject.hpp"

GameObject::GameObject(void) {
    std::memset(&this->_objectParameters, 0, sizeof(this->_objectParameters));
    std::memset(&this->_windowParameters, 0, sizeof(this->_windowParameters));
}

GameObject::GameObject(GameObjectWindowParameters const &window,
        GameObjectParameters const &object) {
     std::memcpy(&this->_objectParameters, &object, sizeof(this->_objectParameters));
     std::memcpy(&this->_windowParameters, &window, sizeof(this->_windowParameters));
}

GameObject::GameObject(GameObject const &copy) {
    *this = copy;
}

GameObject::~GameObject() {
    std::memset(&this->_objectParameters, 0, sizeof(this->_objectParameters));
    std::memset(&this->_windowParameters, 0, sizeof(this->_windowParameters));
}

GameObject &GameObject::operator=(GameObject const &rhs) {
	static_cast<void>(rhs);
	return *this;
}

GameObject::GameObjectParameters const &GameObject::getBody(void) const {
    return this->_objectParameters;
}

GameObject::GameObjectWindowParameters const &GameObject::getWindowParameters() const {
    return this->_windowParameters;
}

void    GameObject::setPosition(int x, int y) {
    this->_windowParameters.x = x;
    this->_windowParameters.y = y;
}

void    GameObject::moveToPosition(int dX, int dY) {
    this->_windowParameters.x += dX;
    this->_windowParameters.y += dY;
}

#define HP this->_objectParameters.currentHP 
#define MaxHP this->_objectParameters.maxHP
bool    GameObject::hasHealth(void) const {
    return HP == 0 ? false : true;
}

void    GameObject::restoreHealth(int newHealth) {
    if (HP + newHealth <= MaxHP)
        HP += newHealth;
}

void    GameObject::decreaseHealth(int damage) {
    if (HP - damage >= 0)
        HP -= damage;
}
#undef HP
#undef MaxHP