# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: yskrypny <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/20 13:16:20 by yskrypny          #+#    #+#              #
#    Updated: 2018/01/20 15:47:31 by yskrypny         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#all rule

#Colors tags

GREENTAG:="\033[1;32m"
YELLOWTAG:="\033[1;33m"
REDTAG:="\033[1;31m"
CENDTAG:="\033[0;0m"
#-----------

#Binary name
NAME = ft_retro
#Compiler
CC = clang++
#Compiler flags
CFLAGS = -Wall -Wextra -Werror
LIBFLAGS = -lncurses
LDFLAGS = rcs
#Project structure directories
IDIR = include
ODIR = obj
SDIR = src
#All project sources list (norminette demand)
SRCS = main.cpp
#Creating objects path
OBJS := $(patsubst %.c,$(ODIR)%.o,$(SRCS))

#Compiling object files
$(ODIR)/%.o:$(SDIR)/%.c
	@mkdir -p $(dir $@)
	@echo $(YELLOWTAG)"|\c"
	@$(CC) $(CFLAGS) -I $(IDIR) -I $(LIDIR) -c $< -o $@

#Linking binary, adding libs etc.
$(NAME):$(OBJS)
	@echo $(GREENTAG)"Linking project..."
	@$(CC) -o $@ $^ $(CFLAGS) $(LIBFLAGS) $(LBFLAGS) -pg
	@echo "Done."$(CENDTAG)

#defining order and checking that obj dir exists
#$(OBJS): | $(ODIR)
#creating obj dir if not exist
#$(ODIR):
#	@mkdir -p $(ODIR)

all: $(NAME)

#clean rule
clean:
	@echo $(YELLOWTAG)"Cleaning project..."
	@rm -rf $(ODIR)
	@echo "Done."$(CENDTAG)
#full clean rule
fclean: clean
	@rm -rf $(NAME)
	@echo $(REDTAG)"Project deleted."$(CENDTAG)
#norminette rule
norm:
	@norminette $(SDIR)/*.c
	@norminette $(IDIR)/*.h
#re rule
re: clean all
#phony defend
.PHONY: all clean fclean re norm
