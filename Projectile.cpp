#include "Projectile.hpp"

Projectile::Projectile() : GameObject() {
}

Projectile::Projectile(GameObjectParameters &parameters,
GameObjectWindowParameters &window) : GameObject(window, parameters) {
}

Projectile::Projectile(Projectile const &copy) : GameObject() {
    *this = copy;
}

Projectile &Projectile::operator=(Projectile const &rhs) {
    static_cast<void>(rhs);
    return *this;
}

void Projectile::shift(void) {
    this->_windowParameters.x += this->_windowParameters.dX;
    this->_windowParameters.y += this->_windowParameters.dY;
}

void Projectile::refresh(void) {
    shift();
}

void Projectile::attack(void) {
    return;
}

