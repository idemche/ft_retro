//
// Created by Yaroslav Skrypnyk on 1/20/18.
//

#include "Enemy.hpp"

Enemy::Enemy(void) : GameObject(
(GameObjectWindowParameters){.x = 0, .y = 0, .width = 1, .height = 1, .dX = -1, .dY = 0},
(GameObjectParameters){"<|||", 15, 100, 100, COLOR_RED}) {
    this->_enemyParameters.lastShooted = 0;
    this->_enemyParameters.damage = 15;
}

Enemy::Enemy(EnemyParameters &parameters, int x, int y, int maxHP) : GameObject(
(GameObjectWindowParameters){.x = 0, .y = 0, .width = 1, .height = 1, .dX = -1, .dY = 0},
(GameObjectParameters){"<|||", 15, 100, 100, COLOR_RED}) {
    this->_enemyParameters = parameters;
    this->_windowParameters.x = x;
    this->_windowParameters.y = y;
    this->_objectParameters.maxHP = maxHP;
}

Enemy::~Enemy(void) {}
Enemy::Enemy(Enemy const &copy) {
    *this = copy;
}

void Enemy::attack(void) {

}

void Enemy::refresh(void) {

}