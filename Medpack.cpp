#include "Medpack.hpp"

Medpack::Medpack() : GameObject((Medpack::GameObjectWindowParameters){.x = Level::width - 1,
    .y = arc4random() % Level::height, .dX = -1, .dY = 0, .width = 3, .height = 1},
    (Medpack::GameObjectParameters){.body = "{+}", 0, 0, COLOR_GREEN}) {}

Medpack::Medpack(Medpack const &copy){
    *this = copy;
}
Medpack::~Medpack() {}

Medpack & Medpack::operator=(Medpack const &rhs) {
	static_cast<void>(rhs);
	return *this;
}

void Medpack::attack() {}

void Medpack::refresh(void) {
	this->moveToPosition(this->_windowParameters.dX,
    this->_windowParameters.dY);
}