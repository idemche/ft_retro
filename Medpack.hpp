#ifndef MEDPACK_HPP
# define MEDPACK_HPP

#include "GameObject.hpp"

class Medpack : public GameObject {
    public:
        Medpack(void);
        Medpack(Medpack const &copy);
        ~Medpack();
        Medpack &operator=(Medpack const &rhs);

        void attack(void);
        void refresh(void);
};

#endif