#include "WindowNcurses.hpp"

std::string const WindowNcurses::_winBorders[] = {
		"              ",
		"++++++++++++++",
		"______________",
		"//////////////"
};

WindowNcurses::WindowNcurses() :
		_height(1), _width(1), _startX(0), _startY(0), _style(WINDEFAULT)
{
	windowCreate();
}

WindowNcurses::WindowNcurses(int height, int width) :
		_height(height), _width(width), _startX(0), _startY(0), _style(WINDEFAULT)
{
	windowCreate();
}

WindowNcurses::WindowNcurses(int height, int width, int startY, int startX, eWinStyles style) :
	_height(height), _width(width), _startX(startX), _startY(startY), _style(style)
{
	windowCreate();
}

WindowNcurses::WindowNcurses(const WindowNcurses &objToCopy)
{
	*this = objToCopy;
}


WindowNcurses::~WindowNcurses()
{
	windowDelete();
}

WindowNcurses &WindowNcurses::operator=(WindowNcurses const &obj)
{
	if (this != &obj)
	{
		std::memcpy(this->_win, obj._win, sizeof(&obj._win));
		this->_height = obj.getHeight();
		this->_width = obj.getWidth();
		this->_startX = obj._startX;
		this->_startY = obj._startY;
		this->_style = obj._style;
	}
	return (*this);
}

int WindowNcurses::getHeight() const
{
	return _height;
}

int WindowNcurses::getWidth() const
{
	return _width;
}

WINDOW *WindowNcurses::getWin() const
{
	return _win;
}

void WindowNcurses::windowCreate(void)
{
	//TODO Delete if not empty;
	this->_win = newwin(this->_height, this->_width, this->_startY, this->_startX);
	box(this->_win, 0 , 0);
	wrefresh(this->_win);
}

bool WindowNcurses::checkWindowDim(int x, int y)
{
	return (x >= 0 && x < this->_width && y >= 0 && y < this->_height);
}

void WindowNcurses::redrawWindow(void)
{
	windowDelete();
	windowCreate();
}

void WindowNcurses::sendMsg(const char *msg) const
{
	if (msg != nullptr)
		wprintw(this->_win, msg);
}

void WindowNcurses::windowDelete(void)
{
#define char(x) static_cast<chtype>(WindowNcurses::_winBorders[this->_style].at(x))
	wborder(this->_win, char(0), char(1), '+', char(3), char(4), char(5), char(6), char(7));
	/* The parameters taken are
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window
	 * 3. rs: character to be used for the right side of the window
	 * 4. ts: character to be used for the top side of the window
	 * 5. bs: character to be used for the bottom side of the window
	 * 6. tl: character to be used for the top left corner of the window
	 * 7. tr: character to be used for the top right corner of the window
	 * 8. bl: character to be used for the bottom left corner of the window
	 * 9. br: character to be used for the bottom right corner of the window
	 */
	wrefresh(this->_win);
	delwin(this->_win);
#undef char
}

